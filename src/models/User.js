const Datatypes = require('sequelize');
const sequelize = require('../config/sequelize');

const User = sequelize.define('User', {
    name: {
        type: Datatypes.STRING,
        allowNull: false
    },
    CPF: {
        type: Datatypes.STRING,
        allowNull: false
    },
    phone_number: {
        type: Datatypes.STRING,
        allowNull: false
    },
    payment_method: {
        type: Datatypes.STRING,
        allowNull: false
    },
    email: {
        type: Datatypes.STRING,
        allowNull: false
    },
    date_of_birthday: {
        type: Datatypes.DATEONLY,
        allowNull: false
        
    },
    moderator: {
        type: Datatypes.BOOLEAN,
        allowNull: false
    },
    pet_type: {
        type: Datatypes.STRING,
        allowNull: false
    },
});


User.associate = function (models) {
    User.belongsTo(models.Address)
    User.hasMany(models.Comment)
};


module.exports = User;