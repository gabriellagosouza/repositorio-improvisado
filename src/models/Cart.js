const Datatypes = require('sequelize');
const sequelize = require('../config/sequelize');

const Cart = sequelize.define('Cart', {
    text: {
        type: Datatypes.TEXT,
        allowNull: false
    },
    evaluation: {
        type: Datatypes.DOUBLE,
        allowNull: false
    },   
});


Cart.associate = function (models) {
    Cart.belongsTo(models.User)
};


module.exports = Cart;
